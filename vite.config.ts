import { defineConfig } from 'vite'

export default defineConfig({
	base: '/unknown-pleasures/',
	build: {
    outDir: 'dist/unknown-pleasures'
  }
})
